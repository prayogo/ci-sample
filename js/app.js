var myApp = angular.module('myApp',[]);

myApp.controller('SampleController', ['$scope', function($scope) {
    $scope.number = '0';

    $scope.randomOdd = function() {
    	var rand = Math.floor((Math.random() * 100) + 1);
    	if (rand%2 == 0)
    		rand = rand + 1;
        $scope.number = rand;
    };

    $scope.randomEven = function() {
    	var rand = Math.floor((Math.random() * 100) + 1);
    	if (rand%2 == 1)
    		rand = rand + 1;
        $scope.number = rand;
    };
}]);